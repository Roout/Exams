/*
15. �������������� � ����������� ����� Fraction ��� ������ � �������� �������.
� ������ ������ ���� ���������� ������ ������������:
- �� ���������;
- � ����� ����������;
������ ���� ����������� ��������� ���������: +, -, *, /.
*/

#include <iostream>
using namespace std;

class Fraction {
	int m_Numerator,
		m_Denominator;
public:
	Fraction(int numer, int denom = 1) :
		m_Numerator(numer),
		m_Denominator(denom)
	{
		int gcd = _gcd(m_Numerator, m_Denominator);
		m_Numerator /= gcd;
		m_Denominator /= gcd;
	}

	friend Fraction operator+(Fraction const& lsh, Fraction const& rsh) {
		return Fraction(
			lsh.m_Numerator*rsh.m_Denominator + lsh.m_Denominator* rsh.m_Numerator,
			lsh.m_Denominator*rsh.m_Denominator
		);
	}
	friend Fraction operator-(Fraction const& lsh, Fraction const& rsh) {
		Fraction r = rsh;
		r.m_Numerator *= -1;
		return Fraction(
			lsh.m_Numerator*rsh.m_Denominator + lsh.m_Denominator* rsh.m_Numerator,
			lsh.m_Denominator*rsh.m_Denominator
		);
	}
	friend Fraction operator*(Fraction const& lsh, Fraction const& rsh) {
		return Fraction(
			lsh.m_Numerator*rsh.m_Numerator,
			lsh.m_Denominator*rsh.m_Denominator
		);
	}
	friend Fraction operator/(Fraction const& lsh, Fraction const& rsh) {
		return lsh * Fraction(rsh.m_Denominator,rsh.m_Numerator);
	}
	friend ostream& operator<<(ostream& os, Fraction const& rsh) {
		os << '(' << rsh.m_Numerator << ")/(" << rsh.m_Denominator << ")";
		return os;
	}
	
private:
	int _gcd(int a, int b) {
		while (a&&b) {
			a %= b;
			swap(a, b);
		}
		return a + b;
	}
};

int main() {
	Fraction a{ 10,15 }, b{ 3,2 }, c{ 5,35 }, d{ 12,18 };
	cout << a << endl << b << endl << c << endl << d << endl;
	cout << "a + b =" << a + b << endl;
	cout << "a / b =" << a / b << endl;
	cout << "c * d =" << c * d << endl;
	cout << "d / c =" << d / c << endl;
	system("pause");
	return 0;
}