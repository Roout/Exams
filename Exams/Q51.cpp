/*
51.	������.
����������� ���������, ������� ���������
�� ������� �� ��� ����� A, B � ���������
���������� ������� �� ������. 

� ������ ������ �������� � ��������� ������������ � ���� rez.txt 
(�������� � / �������� B = ..). 

��� ������������� �������������� �������� ������� �� ���� 
��������� ������ �������� � ���� ERROR.txt 
��������� �� ������ � �������� ��������������� ���������.

*/
#include <iostream>
#include <fstream>

using namespace std;

int main() {
	float a, b;
	cin >> a >> b;
	
	fstream file;
	if (b){
		file.open("rez.txt", ios_base::out);
		if (!file.is_open()) return -1;
		file << a << " / " << b << " = " << a / b;
	}
	else {
		file.open("ERROR.txt", ios_base::out);
		if (!file.is_open()) return -1;
		file << "Error: op1 = " << a << "\nop2 = " << b ;
	}
	file.close();
	return 0;
}