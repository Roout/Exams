#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

/*
39.	������.
��������� ������ �������� 20 ���������� ������� � ��������� �� 0 �� 200.
�� ������ ������ ������� ��� ����������� ��������, 
��������������� ������� ��������� �� ��������.
*/

int main() {
	const int size = 20;
	int a[size] = {};
	//init array
	srand(time(0));//reset random generator
	for (int i = 0; i < size; i++) a[i] = rand() % 201;
	
	int selection[size] = {},
		cnt = 0;//number of 3-digit numbers in selection
	//copy all 3-digit numbers to selection
	for (int i = 0; i < size; i++) {
		if (a[i] >= 100) selection[cnt++] = a[i];
	}
	//bubble sort of array a[]
	for (int i = 0; i < cnt; i++) {
		for (int j = cnt - 1; j > i; j--) {
			if (selection[j - 1] < selection[j]) swap(selection[j - 1], selection[j]);
		}
	}
	//output
	cout << "First array: \n";
	for (int i = 0; i < size; i++) cout << a[i] << " ";

	cout << "\nSecond array: \n";
	for (int i = 0; i < cnt; i++) cout << selection[i] << " ";
	system("pause");
	return 0;
}