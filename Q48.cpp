#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
class Calculate {

public:
	struct Expression {
		float op1, op2;//operands
		char token;
	};
	//functional class
	class Parser {
		bool HasBadSymbs(string expr) {
			string normal = "0123456789+-/*^ .,";
			for (char const& c : expr) {
				if (normal.find(c) == string::npos) {
					return true;
				}
			}
			return false;
		}
	public:
		Expression operator()(string expr) {
			if (HasBadSymbs(expr))
				throw exception("������! ��������� ����!");
			/// sequence is important 
			// sequence is according tokens in enum Token
			string tokens = "+-/*^";
			size_t tokenPosition = string::npos;
			bool hasToken = false;
			for (char&token : tokens) {
				tokenPosition = expr.find(token);
				if (tokenPosition != string::npos) {
					//has found token
					hasToken = true;
					//we don't expect any other tokens in expresion 
					break;
					///but still should check them ??
				}
			}
			if (!hasToken) throw exception("������! ��������� ����!");
			Expression expression;
			expression.token = expr[tokenPosition];

			//convert values like 12,4 to 12.4 for work with stof() function
			size_t coma = 0;
			while ((coma = expr.find(',')) != string::npos) {
				expr[coma] = '.';
			}
			try {
				expression.op1 = stof(expr.substr(0, tokenPosition));
				expression.op2 = stof(expr.substr(tokenPosition + 1, string::npos));
			}
			catch (...) {
				throw exception("������! ��������� ����!");
			}
			return expression;
		}
	};
	float  Calc(string expr) {
		Expression ex;
		try {
			ex = Parser()(expr);
		}
		catch (exception) {
			throw;
		}
		float result = 0.f;
		switch (ex.token) {
		case '+': result = ex.op1 + ex.op2; break;
		case '-': result = ex.op1 - ex.op2; break;
		case '*': result = ex.op1 * ex.op2; break;
		case '/': {
			if (ex.op2)
				result = ex.op1 / ex.op2;
			else
				throw exception("������! ��������� ����!");
			break;
		}
		case '^': result = powf(ex.op1, ex.op2); break;
		default: throw exception("������! ��������� ����!");
		}

		return result;
	}
};
int main() {
	setlocale(0, "Rus");
	Calculate calc;
	while (true) {
		char * buffer = new char[255];
		cin.getline(buffer, 255);
		string s(buffer);
		delete[] buffer;
		buffer = nullptr;

		float result = 0;
		try {
			result = calc.Calc(s);
		}
		catch (exception& ex) {
			cout << ex.what();
			continue;
		}
		cout << result << endl;
	}
	return 0;
}