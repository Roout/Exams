#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
/*
42.	Task.
Sort every even column in 2D Matrix 10x10 using bubble-sort.
*/
const int sz = 10;
void Out(int (&m)[sz][sz]) {
	for (int i = 0; i < sz; i++) {
		for (int j = 0; j < sz; j++) {
			cout.width(4);
			cout<< m[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}
int main() {
	int m[sz][sz] = {};
	//init matrix by random values
	srand(time(0));
	for (int i = 0; i < sz; i++)
		for (int j = 0; j < sz; j++)
			m[i][j] = rand() % 201;
	//ouput
	Out(m);
	//bubble sort
	for (int i = 0; i < sz; i ++) {//columns
		if (i & 1) {//is even if we count from 1 instead of 0
			//sort column i
			for (int k = 0; k < sz; k++) {
				for (int j = sz - 1; j > k; j--) {
					if (m[j - 1][i] < m[j][i]) swap(m[j - 1][i], m[j][i]);
				}
			}
			//end sorting
		}//end if
	}//end for
	 //ouput
	Out(m);
	system("pause");
	return 0;
}